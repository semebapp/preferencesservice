package com.semebapp.preferencesservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PreferencesserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(PreferencesserviceApplication.class, args);
    }

}
