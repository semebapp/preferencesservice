package com.semebapp.preferencesservice.controller;


import com.semebapp.preferencesservice.entities.Preferences;
import com.semebapp.preferencesservice.services.PreferencesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value = "preferences")
public class PreferencesController {
    @Autowired
    private PreferencesService preferencesService;

    @GetMapping("/{id}")
    public @ResponseBody
    Preferences getPreferences(@PathVariable String id) {
        return preferencesService.getPreferences(id);
    }

    @PostMapping
    public @ResponseBody
    Preferences createPreferences(@RequestBody Preferences preferences) {
        return preferencesService.createPreferences(preferences);
    }

    @DeleteMapping("/{id}")
    public @ResponseBody
    Preferences deletePreferences(@PathVariable String id) {
        return this.preferencesService.deletePreferences(id);
    }

    @PutMapping("/{id}")
    public @ResponseBody
    Preferences updatePreferences(@RequestBody Preferences preferences, @PathVariable String id) {
        return this.preferencesService.updatePreferences(preferences, id);
    }

    @GetMapping("/health")
    public @ResponseBody
    ResponseEntity<String> health() {
        return ResponseEntity.ok("OK");
    }
}
