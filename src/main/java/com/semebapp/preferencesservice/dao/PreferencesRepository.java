package com.semebapp.preferencesservice.dao;

import com.semebapp.preferencesservice.entities.Preferences;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PreferencesRepository extends JpaRepository<Preferences, String> {
    
}
