package com.semebapp.preferencesservice.entities;

import javax.persistence.*;

@Table(name = "preferences")
@Entity
public class Preferences {

    @Id
    private String user;

    private String currencyCode;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }
}
