package com.semebapp.preferencesservice.services;

import com.semebapp.preferencesservice.entities.Preferences;
import org.springframework.stereotype.Service;

@Service
public interface PreferencesService {


    Preferences getPreferences(String id);

    Preferences createPreferences(Preferences account);

    Preferences updatePreferences(Preferences account, String id);

    Preferences deletePreferences(String id);
}
