package com.semebapp.preferencesservice.services;

import com.semebapp.preferencesservice.dao.PreferencesRepository;
import com.semebapp.preferencesservice.entities.Preferences;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PreferencesServiceImpl implements PreferencesService {

    @Autowired
    private PreferencesRepository repository;

    @Override
    public Preferences getPreferences(String id) {
        Optional<Preferences> preferences = repository.findById(id);
        if (preferences.isPresent()) {
            return preferences.get();
        }
        return null;
    }

    @Override
    public Preferences createPreferences(Preferences preferences) {
        return repository.save(preferences);
    }

    @Override
    public Preferences updatePreferences(Preferences preferences, String id) {
        return repository.save(preferences);
    }

    @Override
    public Preferences deletePreferences(String id) {
        repository.deleteById(id);
        return null;
    }
}
